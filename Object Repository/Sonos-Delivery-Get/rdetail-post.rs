<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>rdetail-post</name>
   <tag></tag>
   <elementGuidId>6703a0c9-7a0b-42a3-9f33-6087d006d1d9</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\&quot;Name\&quot;:\&quot;KBOBS\&quot;,\n\&quot;Fullname\&quot;:\&quot;KBOBS\&quot;,\n\&quot;Email\&quot;:\&quot;kbobs@gmail.com\&quot;,\n\&quot;Password\&quot;:\&quot;XYZQ\&quot;,\n\&quot;Number\&quot;:\&quot;965258742\&quot;,\n\&quot;GST_Id\&quot;:\&quot;1\&quot;,\n\&quot;Business_Name\&quot;:\&quot;Food Industry\&quot;,\n\&quot;Tax_Slab\&quot;:\&quot;5\&quot;,\n\&quot;Restaurant_Id\&quot;:\&quot;1\&quot;\n}\n&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://q7n942kcc0.execute-api.us-east-1.amazonaws.com/Prod/rdetail</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()



WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
