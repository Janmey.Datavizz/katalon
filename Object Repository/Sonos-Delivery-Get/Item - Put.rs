<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Item - Put</name>
   <tag></tag>
   <elementGuidId>68f751cf-e7f4-4bce-90c3-1b5778f77c14</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\&quot;Name\&quot;: \&quot;Chicken 123\&quot;, \&quot;Category_Id\&quot;: 1, \&quot;Type_Id\&quot;: 1, \&quot;Cost\&quot;: 300, \&quot;ChefSpecial_Status\&quot;: 0, \&quot;MenuSpecial_Status\&quot;: 0, \&quot;Description\&quot;: \&quot;\&quot;, \&quot;stuffing\&quot;: {\&quot;1\&quot;: {\&quot;stuffing_id\&quot;: 1, \&quot;cost\&quot;: 200}, \&quot;2\&quot;: {\&quot;stuffing_id\&quot;: 2, \&quot;cost\&quot;: 400}}}&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>text/plain</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>https://q7n942kcc0.execute-api.us-east-1.amazonaws.com/Prod/item</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()



WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
