<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>restaurantaddress-put</name>
   <tag></tag>
   <elementGuidId>ea783fea-d59e-42e2-8b4b-884c978b2b59</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\n \&quot;Restaurant_AddressId\&quot;:4,\n\n \&quot;Restaurant_Id\&quot;:1,\n\n \&quot;Fullname\&quot;:\&quot;payal\&quot;,\n\n \&quot;Email\&quot;:\&quot;wadhwanipayal102@gmail.com\&quot;,\n\n \&quot;Password\&quot;:\&quot;7188\&quot;,\n\n \&quot;Contact_Number\&quot;:8529637411,\n\n \&quot;Branch_Name\&quot;:\&quot;Maninagar\&quot;,\n\n \&quot;Branch_Location\&quot;:\&quot;Rambaug\&quot;,\n\n \&quot;Latitude\&quot;:22.7083,\n\n \&quot;Longitude\&quot;:72.8565,\n\n \&quot;Status\&quot;:1,\n\n \&quot;EDT\&quot;:30,\n\n \&quot;Delivery_Radius\&quot;:5,\n\n \&quot;Delivery_Charge\&quot;:20,\n\n \&quot;Delivery_Option\&quot;:1,\n\n \&quot;Payment_Option\&quot;:\&quot;all\&quot;,\n\n \&quot;Timings\&quot;:[\n\n    {\n\n       \&quot;Day\&quot;:\&quot;Monday\&quot;,\n\n       \&quot;Pickup\&quot;:{\n\n          \&quot;Start\&quot;:\&quot;9:00 AM\&quot;,\n\n          \&quot;End\&quot;:            \&quot;9:00 PM\&quot;\n\n       },\n\n       \&quot;Delivery\&quot;:{\n\n          \&quot;Start\&quot;:            \&quot;9:00 AM\&quot;,\n\n          \&quot;End\&quot;:            \&quot;9:00 PM\&quot;\n\n       }\n\n    },\n\n    {\n\n       \&quot;Day\&quot;:\&quot;Tuesday\&quot;,\n\n       \&quot;Pickup\&quot;:{\n\n          \&quot;Start\&quot;:            \&quot;9:00 AM\&quot;,\n\n          \&quot;End\&quot;:            \&quot;9:00 PM\&quot;\n\n       },\n\n       \&quot;Delivery\&quot;:{\n\n          \&quot;Start\&quot;:            \&quot;9:00 AM\&quot;,\n\n          \&quot;End\&quot;:            \&quot;9:00 PM\&quot;\n\n       }\n\n    },\n\n    {\n\n       \&quot;Day\&quot;:\&quot;Wednesday\&quot;,\n\n       \&quot;Pickup\&quot;:{\n\n          \&quot;Start\&quot;:            \&quot;9:00 AM\&quot;,\n\n          \&quot;End\&quot;:            \&quot;9:00 PM\&quot;\n\n       },\n\n       \&quot;Delivery\&quot;:{\n\n          \&quot;Start\&quot;:            \&quot;9:00 AM\&quot;,\n\n          \&quot;End\&quot;:            \&quot;9:00 PM\&quot;\n\n       }\n\n    },\n\n    {\n\n       \&quot;Day\&quot;:\&quot;Thursday\&quot;,\n\n       \&quot;Pickup\&quot;:{\n\n          \&quot;Start\&quot;:            \&quot;9:00 AM\&quot;,\n\n          \&quot;End\&quot;:            \&quot;9:00 PM\&quot;\n\n       },\n\n       \&quot;Delivery\&quot;:{\n\n          \&quot;Start\&quot;:            \&quot;9:00 AM\&quot;,\n\n          \&quot;End\&quot;:            \&quot;9:00 PM\&quot;\n\n       }\n\n    },\n\n    {\n\n       \&quot;Day\&quot;:\&quot;Friday\&quot;,\n\n       \&quot;Pickup\&quot;:{\n\n          \&quot;Start\&quot;:            \&quot;9:00 AM\&quot;,\n\n          \&quot;End\&quot;:            \&quot;9:00 PM\&quot;\n\n       },\n\n       \&quot;Delivery\&quot;:{\n\n          \&quot;Start\&quot;:            \&quot;9:00 AM\&quot;,\n\n          \&quot;End\&quot;:            \&quot;9:00 PM\&quot;\n\n       }\n\n    },\n\n    {\n\n       \&quot;Day\&quot;:\&quot;Saturday\&quot;,\n\n       \&quot;Pickup\&quot;:{\n\n          \&quot;Start\&quot;:            \&quot;9:00 AM\&quot;,\n\n          \&quot;End\&quot;:            \&quot;9:00 PM\&quot;\n\n       },\n\n       \&quot;Delivery\&quot;:{\n\n          \&quot;Start\&quot;:            \&quot;9:00 AM\&quot;,\n\n          \&quot;End\&quot;:            \&quot;9:00 PM\&quot;\n\n       }\n\n    },\n\n    {\n\n       \&quot;Day\&quot;:\&quot;Sunday\&quot;,\n\n       \&quot;Pickup\&quot;:{\n\n          \&quot;Start\&quot;:            \&quot;9:00 AM\&quot;,\n\n          \&quot;End\&quot;:            \&quot;9:00 PM\&quot;\n\n       },\n\n       \&quot;Delivery\&quot;:{\n\n          \&quot;Start\&quot;:            \&quot;9:00 AM\&quot;,\n\n          \&quot;End\&quot;:            \&quot;9:00 PM\&quot;\n\n       }\n\n    }\n\n ]\n\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>https://q7n942kcc0.execute-api.us-east-1.amazonaws.com/Prod/restaurantaddress</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()



WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
